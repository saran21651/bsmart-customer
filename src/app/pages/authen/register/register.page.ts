import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private route: Router,
  ) { 
    
  }

  @ViewChild("input_username", {static: false}) i_username;
  @ViewChild("input_password", {static: false}) i_password;
  @ViewChild("input_confirmpass", {static: false}) i_confirmpass;
  @ViewChild("input_fname", {static: false}) i_fname;
  @ViewChild("input_lname", {static: false}) i_lname;
  @ViewChild("input_email", {static: false}) i_email;
  @ViewChild("input_line", {static: false}) i_line;

  ngOnInit() {
  }

  public Register_Click () {

    if (this.i_password.value != this.i_confirmpass.value) {
      alert("error ! password not match");
      return;
    }

    let data = {
      username: this.i_username.value,
      password: this.i_password.value,
      fname: this.i_fname.value,
      lname: this.i_lname.value,
      email: this.i_email.value,
      line: this.i_line.value
    } 

    this.auth.Register(data)
      .then(result => {
        console.log(result);
        if (result["code"] == 400) {
          alert(result["message"]);
        } else if (result["code"] == 200) {
          alert(result["message"]);
          this.route.navigate(['/login']);
        } else {
          alert("error");
        }
      });
  }

  public Back_Click () {
    this.route.navigate(['/login']);
  }
}
