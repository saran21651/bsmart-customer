import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private auth: AuthenticationService,
    private route: Router,
    private storage: StorageService
  ) { }

  @ViewChild("input_phone", {static: false}) i_phone;
  @ViewChild("input_password", {static: false}) i_pass; 

  ngOnInit() {
  }

  public Login_Click () {
    let data = {
      username: this.i_phone.value,
      password: this.i_pass.value
    }

    this.auth.Login(data).then(result => {
      console.log(result);
      if (result["code"] == 400) {
        alert(result["message"]);
      } else if (result["code"] == 200) {
        alert(result["message"]);
        let value = {
          id: result["customer_app_id"],
          username: result["customer_app_username"],
          name_th: result["customer_app_name_th"],
          name_en: result["customer_app_name_en"],
          last_name_th: result["customer_app_last_name_th"],
          last_name_en: result["customer_app_last_name_en"],
          line: result["customer_line"],
          email: result["customer_email"],
        };
        this.storage.setUserInformation(value);
        this.route.navigate(['/home']);
      } else {
        alert("error");
      }
    });
  }

  public Register_Click () {
    this.route.navigate(['/register']);
  }

}
