import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  private data;
  private data_array;

  // เก็บข้อมูลจาก page ที่ต้องการจะส่ง
  public setData (data_from_page) {
    this.data = data_from_page;
  }

  // ดึงข้อมูลไปใช้ใน page ใหม่
  public getData () {
    return this.data;
  }

  public setDataArray (data_from_page) {
    this.data_array = [];
    this.data_array = data_from_page;
  }

  public getDataArray () {
    return this.data_array;
  }
}
