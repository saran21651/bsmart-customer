import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  private path = {
    USER_INFO: "user::information"
  }

  public setUserInformation (data) {
    let value = JSON.stringify(data);
    localStorage.setItem(this.path.USER_INFO, value);
  }

  public getUserInformation () {
    let value = localStorage.getItem(this.path.USER_INFO);
    return JSON.parse(value);
  }

  public clearStorage () {
    localStorage.clear();
  }
}
