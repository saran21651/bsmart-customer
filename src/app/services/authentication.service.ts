import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private http: HttpClient,
    private http_native: HTTP
  ) { }

  private url = "http://10.0.0.100/poms/api.php";

  public Login (data)
  {
    return new Promise<any>((resolve) => {
      let body = {
        _compgrp: "commons",
        _comp: "api",
        _action: "CustomerLogin",
        username: data["username"],
        password: data["password"]
      };

      this.http.post(this.url, body, {})
        .subscribe(result => {
          resolve(result);
        }, err => {
          console.log(err);
          resolve("error");
        });
    });
  }

  public Register (data)
  {
    return new Promise<any>((resolve) => {
      
      let body = {
        _compgrp: "commons",
        _comp: "api",
        _action: "CustomerRegister",
        username: data["username"],
        password: data["password"],
        fname: data["fname"],
        lname: data["lname"],
        email: data["email"],
        line: data["line"]
      };

      this.http.post(this.url, body, {})
        .subscribe(result => {
          resolve(result);
        }, err => {
          console.log(err);
          resolve("error");
        });
    });
  }
}
